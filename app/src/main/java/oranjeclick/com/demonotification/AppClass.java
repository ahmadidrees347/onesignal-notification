package oranjeclick.com.demonotification;

import android.app.Application;

        import com.google.firebase.FirebaseApp;
        import com.onesignal.OneSignal;

public class AppClass extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        FirebaseApp.initializeApp(this);
        // OneSignal Initialization
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();
    }
}